Reference of package `sftputil`.

Modules:

- [sftputil](sftputil.md)
- [sync](sync.md)
- [glob](glob.md)

::: sftputil
